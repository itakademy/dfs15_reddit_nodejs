const run = () => {
  const search = document.getElementById('search');
  if(search) {
    search.addEventListener('submit', event => {
      event.preventDefault();
      event.stopPropagation();
      const search = event.target.querySelector('input[type=search]') ;
      fetch('/posts?search='+search.value).then(result => result.json()).then(result => {
        if(result.ok && result.nbResults) {
          document.getElementById('container').innerHTML = '' ;
          result.result.forEach(l => {
            document.getElementById('container').innerHTML += l ;
          })
        }
        search.value = "";
      })
    })
  }
  
  const forms = document.querySelectorAll('#loginForms form');
  if(forms && forms.length) {
    forms.forEach(l => {
      l.addEventListener('submit', evt => {
        evt.preventDefault();
        evt.stopPropagation();
        
        const form = evt.target ;
        const pseudo = (form.querySelector('[type=text]') || {}).value;
        const email = (form.querySelector('[type=email]') || {}).value;
        const password = (form.querySelector('[type=password]') || {}).value;
        
        fetch(form.getAttribute('action'), {
          method : form.getAttribute('method'),
          credentials: 'same-origin',
          headers: {
            'Content-Type': 'application/json'
          },
          body : JSON.stringify({
            email, password, pseudo
          })
        }).then(result => result.json()).then(result => {
          if(result.ok )
            return document.location.reload() ;
          document.getElementById('error_connexion').classList.add('show') ;
          window.setTimeout(()=> {
            document.getElementById('error_connexion').classList.remove('show') ;
          }, 500)
          search.value = "";
        })
      })
    })
  }
  
  const form = document.getElementById('addPost');
  if(form) {
      form.addEventListener('submit', evt => {
        evt.preventDefault();
        evt.stopPropagation();
        let datas = {} ;
        
        evt.target.querySelectorAll('[name]').forEach(l => {
          if(l && l.value) {
            if(l.type == 'radio' && !l.checked)
              return ;
            datas[l.name] = l.value ;
          }
        })
        fetch(evt.target.getAttribute('action'), {
          method : evt.target.getAttribute('method'),
          credentials: 'same-origin',
          headers: {
            'Content-Type': 'application/json'
          },
          body : JSON.stringify(datas)
        }).then(result => result.json()).then(result => {
          if(result.ok)
            return document.location.reload() ;
          document.getElementById('error_connexion').classList.add('show') ;
          window.setTimeout(()=> {
            document.getElementById('error_connexion').classList.remove('show') ;
          }, 500)
          search.value = "";
        })
      })
  }
  
  const rateButtons = document.querySelectorAll('.rateUp, .rateDown')
  if(rateButtons && rateButtons.length) {
    rateButtons.forEach((l => {
      l.addEventListener('click', evt => {
        let button = evt.target ;
        let element = button.closest('[data-id]') ;
        let id = element.dataset.id  ;
        
        console.log(button)
        console.log(element)
        console.log(id)
        fetch('/posts/'+id, {
          method : 'PUT',
          credentials: 'same-origin',
          headers: {
            'Content-Type': 'application/json'
          },
          body : JSON.stringify({rate:button.dataset.rate})
        }).then(result => result.json()).then(result => {
          if(result.ok)
            return document.location.reload() ;
          document.getElementById('error_connexion').classList.add('show') ;
          window.setTimeout(()=> {
            document.getElementById('error_connexion').classList.remove('show') ;
          }, 500)
          search.value = "";
        })
      })
    }))
  }


  (elements => {
    if(!elements || !elements.length)
      return ;
    elements.forEach(l => {
      let url = l.getAttribute('datasrc');

    })
  })(document.querySelectorAll('a[datasrc].link'))
  
};



window.addEventListener('DOMContentLoaded',run) ;