const express = require('express');
const router = express.Router();
const createError = require('http-errors');
const crypto = require('crypto');
const mongo = require('../bin/mongo') ;
const ObjectId = require('mongodb').ObjectId ;

const defaultAvatar = "https://cdn2.vectorstock.com/i/thumb-large/20/76/man-avatar-profile-vector-21372076.jpg";
/**
 * get user details
 */
router.get('/:id', (req, res) => {
  // requete dans BDD sur user { _id : ObjectId( req.params.id ) }
  mongo.getInstance().collection('users').findOne(
    { _id : ObjectId( req.params.id ) },
    (err, result) => {
      if(err) throw err ;
      res.send({ok:true, result : result }) ;
  });
});

/**
 * update me
 */
router.put('/:id', (req, res, next) => {
  // if session._id exists
  if(!req.session.user._id || req.session.user._id !== req.params.id) {
    return next(createError(403))
  }
  let datas = {} ;
  let properties = ['avatar', 'description', 'pseudo', 'settings'];
  for (var i in properties) {
    if(req.body[properties[i]]) {
      datas[properties[i]] = req.body[properties[i]] ;
    }
  }
  if(Object.keys(datas).length >= 1) {
    datas.lastUpdate = new Date();
    mongo.getInstance().collection('users').updateOne(
      { _id : ObjectId( req.params.id ) },
      { $set : datas },
      (err, result) => {
        if(err) throw err ;
        res.send({ok:true, result : result }) ;
      });
  }
});


router.get('/', function(req, res, next) {
  let datas = {user : (req.session.user || {}), title:'Espace client'} ;
  
  if(req.session.user && req.session.user._id) {
    mongo.getInstance().collection('posts').count({'author._id': ObjectId(req.session.user._id), archive:{$ne:true}, $or : [{parent_id: ''}, {parent_id: {$exists:0}}], archive:{$ne:true}}, (err, nbPosts) => {
      mongo.getInstance().collection('posts').count({'author._id': ObjectId(req.session.user._id), archive:{$ne:true}, parent_id:{$exists:1}, archive:{$ne:true}}, (err, nbComments) => {
        mongo.getInstance().collection('posts').find({'author._id': ObjectId(req.session.user._id), archive:{$ne:true}, $or : [{parent_id: ''}, {parent_id: {$exists:0}}]}).sort({DateTime: -1}).limit(3).toArray((err, posts) => {
          let nbFriends = (req.session.user.friends || []).length
          let nbSub = (req.session.user.sub || []).length
          res.render('user', {...datas, posts, nbPosts, nbComments, nbFriends, nbSub});
        })
      })
    })
  } else {
    res.render('user', datas);
  }
});

/**
 * création d'un utilisateur
 */
router.post('/', (req, res, next) => {
  // verifier les données reçues en post
  if(req.body && req.body.email && req.body.pseudo && req.body.password)
  // verifier que le mail n'est pas en BDD
    mongo.getInstance().collection('users').findOne({email:req.body.email}, (err, user) => {
      if(err) throw err ;
      if(user && user._id) {
        return next(createError(406));
      }
      // créer l'objet user à insérer en BDD
      let crytedPassword = crypto.createHash('sha256').update(req.body.password).digest('hex') ;
      let newUser = {
        pseudo : req.body.pseudo,
        mail :req.body.email,
        password : crytedPassword,
        avatar : defaultAvatar,
        description : "",
        creationDate:new Date(),
        lastUpdate : new Date(),
        lastConnect : "",
        settings : {
        }
      };
      // inserer l'objet user en BDD
      mongo.getInstance().collection('users').insertOne(newUser, (err, result) => {
        if(err) throw err ;
        // verifier qu'il a bien été inséré
        if(result.insertedId){
          // repond au client
          req.session.user = newUser ;
          req.session.user._id = String(result.insertedId) ;
          return res.send({ok:true, message:'utilisateur créé'});
        }
        else
          return next(createError(402))
      });
    });
});

/**
 * login
 */
router.put('/', (req, res, next) => {
  console.log(req.body)
  if(req.body && req.body.email && req.body.password) {
    let crytedPassword = crypto.createHash('sha256').update(req.body.password).digest('hex') ;
    mongo.getInstance().collection('users').findOne({
      mail : req.body.email,
      password : crytedPassword
    }, (err, user) => {
      if(err) throw err ;
      if(user && user._id) {
        // create session
        req.session.user = Object.assign({}, user) ;
        req.session.user._id = String(user._id) ;
        res.send({ok:true})
      } else {
        return next(createError(401))
      }
    })
  } else {
    return next(createError(401))
  }
});
/**
 * logout
 */
router.delete('/', (req, res) => {
  // session destroy
  delete req.session.user;
  res.send({ok:true}) ;
});

    

module.exports = router;
