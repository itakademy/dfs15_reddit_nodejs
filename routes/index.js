var express = require('express');
var router = express.Router();
const mongo = require('../bin/mongo');

/* GET home page. */
router.get('/', function (req, res, next) {
  mongo.getInstance().collection('posts').find({archive:{$ne:true}, $or : [{parent_id: ''}, {parent_id: {$exists:0}}]}).sort({rate: -1}).limit(3).toArray((err, trending) => {
    mongo.getInstance().collection('posts').find({archive:{$ne:true}, $or : [{parent_id: ''}, {parent_id: {$exists:0}}]}).sort({dateTime: -1}).limit(50).toArray((err, posts) => {
      mongo.getInstance().collection('posts').distinct( "sub",  {archive:{$ne:true}}, (err, subs) => {
        res.render('index', {title: 'It-Reddit', trending, posts, user: (req.session.user || {}), subs});
      })
    })
  })
});

module.exports = router;
