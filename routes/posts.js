const express = require('express');
const router = express.Router();
const createError = require('http-errors');
const mongo = require('../bin/mongo') ;
const ObjectId = require('mongodb').ObjectId ;
const twig = require('twig').twig;


// router.use('/', (req, res, next) => {
//   console.log(req.session.user)
//   console.log(req.cookies)
// })
//
/**
 * get post details
 */
router.get('/:id', (req, res, next) => {
  mongo.getInstance()
    .collection('posts')
    .findOne(
      {_id : ObjectId(req.params.id)},
      (err, post) => {
        if(err) throw err ;
        if(!post || !post._id)
          next(createError(404))
        res.send({ok:true, result : post}) ;
      })
});

/**
 * update post details if mine
 * add vote to post
 */
router.put('/:id', (req, res, next) => {
  if(!req.session || !req.session.user || !req.session.user._id) {
    next(createError(401)) ;
  }
  mongo.getInstance()
    .collection('posts')
    .findOne(
      {_id : ObjectId(req.params.id)},
      (err, post) => {
        if(err) throw err ;
        if(!post || !post._id)
          next(createError(404));
        const datas = {};
        if(String(post.author._id) === req.session._id) {
          // it's mine
          const options = ['message', 'title', 'sub'] ;
          for (var i in options) {
            if(req.body[options[i]]) {
              datas[options[i]] = req.body[options[i]] ;
            }
          }
          if(req.body['media.type'] &&  req.body['media.url']) {
            datas['media'] = {
              type:req.body['media.type'],
              url: req.body['media.url']
            } ;
          }
          if(Object.keys(datas).length) {
            datas.lasUpdate = new Date() ;
            mongo.getInstance()
              .collection('posts')
              .updateOne({_id : ObjectId(req.params.id)},{$set : datas}, (err, result) => {
                res.send({ok:true});
              });
          }
        } else {
          if(req.body.rate) {
            const datas = {
              rate: parseInt(req.body.rate),
              userId : ObjectId(req.session._id),
              dateTime : new Date()
            };
            post.rate = datas.rate ;
            for (var i in (post.usersRate ||[])) {
              post.rate += post.usersRate[i].rate ;
            }
            post.usersRate.push(datas) ;
            post.lasUpdate = new Date() ;
            mongo.getInstance()
              .collection('posts')
              .updateOne({_id : ObjectId(req.params.id)},{
                  $inc : {rate:parseInt(req.body.rate)},
                  $push : {usersRate : datas},
                  $set : {lasUpdate : new Date()}
                }, (err, result) => {
                res.send({ok:true});
              });
          }
        }
      })
});

/**
 * add comment on post
 */
router.post('/:id', (req, res) => {
  if(!req.session || !req.session.user || !req.session.user._id) {
    next(createError(401)) ;
  }
  mongo.getInstance()
    .collection('posts')
    .findOne(
      {_id : ObjectId(req.params.id)},
      (err, post) => {
        if(err) throw err ;
        if(!post || !post._id)
          next(createError(404));
        // le post d'origine n'existe plus ...

        if(req.body.message) {
          const datas = {
            parent_id: ObjectId(req.params.id),
            message: req.body.message,
            author: {
              _id: ObjectId(req.session.user._id),
              pseudo: req.session.user.pseudo,
              avatar: req.session.user.avatar
            },
            dateTime: new Date()
          };
          mongo.getInstance()
            .collection('posts')
            .insertOne(datas, (err, result) => {
              mongo.getInstance()
                .collection('posts')
                .updateOne(
                  {_id : ObjectId(req.params.id)},
                  { $inc: { nbComments: +1}},
                  (err, result) => {
                    res.send({ok:true});
                })
            });
          }
      });
});

/**
 * delete post
 */
router.delete('/:id', (req, res) => {
  mongo.getInstance()
    .collection('posts')
    .updateOne({_id : ObjectId(req.params.id)},{$set : {archive:true}}, (err, result) => {
      res.send({ok:true});
    });
});

/**
 * get all post
 * filtrable ^^
 */
router.get('/', function(req, res, next) {
  let query = {
    archive: {$ne : true},
    $or : [{parent_id: ''}, {parent_id: {$exists:0}}]
  } ;
  let limit = 20;
  let skip = 0;
  if(req.query) {
    for (var i in req.query) {
      if(!req.query[i])
        continue ;
      switch(i) {
        case "search" :
          query.$and = [{$or : query.$or} , {$or : [
            {title : {$regex: `.*${req.query.search}.*`, $options:"gi"}},
            {message : {$regex: `.*${req.query.search}.*`, $options:"gi"}},
          ]}] ;
          delete query.$or;
          break ;
        case "sub" :
          query.sub = {
            $in : req.query.sub.map ? req.query.sub : [req.query.sub]
          };
          break ;
        case "author" :
          query['author._id'] = ObjectId(req.query[i]);
          break ;
        case "minDate" :
          if(!query.dateTime) {
            query.dateTime = {}
          }
          query.dateTime.$gte = new Date(req.query[i]);
          break ;
        case "maxDate" :
          if(!query.dateTime) {
            query.dateTime = {}
          }
          query.dateTime.$lte = new Date(req.query[i]);
          break ;
        case "skip" :
          skip = req.query[i];
          break ;
        case "limit" :
          limit = req.query[i];
          break ;
      }
    }
  }
  mongo.getInstance().collection("posts").count(query, (err, nbResults) => {
    mongo.getInstance().collection("posts").find(query).skip(skip).limit(limit).sort({dateTime: -1}).limit(50).toArray((err, posts) => {
      console.log(req.ips)
      if(req.query.fromApp) {
        res.send({ok:true, nbResults, result:posts});
      } else {
        let result = [] ;
        console.log(twig)
        for (var i in posts) {
          res.render('post.twig', {post:posts[i]}, (err, html) => {
            result.push(html) ;
          });
        }
        res.send({ok:true, nbResults, result});
      }
    });
  })
});

/**
 * création d'un post
 */
router.post('/', (req, res, next) => {
  if(!req.session || !req.session.user || !req.session.user._id) {
    next(createError(401)) ;
  }

  var datas = {
    title : req.body.title,
    message : req.body.message,
    sub : req.body.sub,
    dateTime : new Date(),
    lastUpdate : new Date(),
    author : {
      _id: ObjectId(req.session.user._id),
      pseudo: req.session.user.pseudo,
      avatar: req.session.user.avatar
    },
    rate:0,
    usersRate: [],
    nbComments:0
  };
  if(req.body['media.type'] &&  req.body['media.url']) {
    datas.media = {
      type:req.body['media.type'],
      url: req.body['media.url']
    } ;
  }
  mongo.getInstance().collection('posts').insertOne(datas, (err, result) => {
    if(err) throw err;
    if(datas.media && datas.media.type && datas.media.type === 'link') {
      var parser = require("ogp-parser");
      parser(datas.media.url, false).then(function(data) {
        let meta = {...data} ;
        meta.ogp = {} ;
        for(var i in data.ogp) {
          meta.ogp[i.split(':')[1]] = data.ogp[i];
        }
        console.log({$set:{'media.meta':meta}})
        mongo.getInstance().collection('posts').updateOne({_id : result.insertedId}, {$set:{'media.meta':meta}}, (err, upsult) => {
          if(err) throw err;
          res.send({ok:true, result}) ;
        })
      }).catch(function(error) {
        console.error(error);
      });
    } else {
      res.send({ok:true, result}) ;
    }
  })
});


module.exports = router;
